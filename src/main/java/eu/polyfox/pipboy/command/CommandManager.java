package eu.polyfox.pipboy.command;

import eu.polyfox.pipboy.PipBoy;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Maintains command invocation associations.
 * @author tr808axm
 */
public class CommandManager extends ListenerAdapter {
    private final Logger logger = Logger.getLogger(getClass().getName());
    private final PipBoy pipBoy;
    private final Map<String, CommandHandler> commandAssociations = new HashMap<>();

    /**
     * Constructs and registers the command manager.
     * @param pipBoy the framework instance.
     */
    public CommandManager(PipBoy pipBoy) {
        this.pipBoy = pipBoy;
        pipBoy.getJda().addEventListener(this);
    }

    /**
     * Event handler used to catch command invocations.
     */
    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!event.getAuthor().isBot() && !event.getAuthor().isFake() && !event.isWebhookMessage()) {
            CommandInvocationContext context = CommandInvocationContext.parse(pipBoy, event.getMessage());
            if(context != null)
                call(context);
        }
    }

    /**
     * Calls the {@link CommandHandler} associated with the command.
     * @param context the parsed command invocation and it's context.
     */
    private void call(CommandInvocationContext context) {
        CommandHandler commandHandler = getCommandHandler(context.getUsedAlias());
        Message response;
        if(commandHandler == null)
            response = null; // TODO command not found message
        else {
            context.setHandler(commandHandler);
            response = commandHandler.call(context);
        }

        // respond
        if(response != null)
            context.getChannel().sendMessage(response).queue(msg -> msg.delete().queueAfter(30, TimeUnit.SECONDS));

        // delete invocation message on guilds
        if(context.getGuild() != null && context.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE))
            context.getMessage().delete().queue();
    }

    /**
     * @param alias the key property to the CommandHandler.
     * @return the associated CommandHandler or null if none is associated.
     */
    private CommandHandler getCommandHandler(String alias) {
        return commandAssociations.get(alias);
    }

    /**
     * @return a clone of all registered command associations.
     */
    public Map<String, CommandHandler> getCommandAssociations() {
        return new HashMap<>(commandAssociations);
    }

    /**
     * Registers the command handlers aliases.
     * @param commandHandler the command handler whose aliases should be registered.
     */
    public void registerCommandHandler(CommandHandler commandHandler) {
        for (String alias : commandHandler.getAliases())
            if(commandAssociations.containsKey(alias))
                logger.warning("'" + commandHandler.getClass().getName() + "' command handler tried to register alias '"
                        + alias + "' which is already taken by '" + commandAssociations.get(alias).getClass().getName() + "'.");
            else
                commandAssociations.put(alias, commandHandler);
    }
}
