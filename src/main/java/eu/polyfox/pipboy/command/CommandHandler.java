package eu.polyfox.pipboy.command;

import eu.polyfox.pipboy.plugin.Plugin;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.util.logging.Logger;

/**
 * Handles a specific command.
 * @author tr808axm
 */
public abstract class CommandHandler {
    private final Logger logger = Logger.getLogger(getClass().getName());
    private final Plugin plugin;
    private final String[] aliases;
    private final String description;
    private final String usage;

    /**
     * Constructs a new CommandHandler.
     * @param plugin the plugin this command belongs to.
     * @param aliases all invocation aliases.
     * @param description a short description for the command.
     * @param usage parameter usage code.
     */
    public CommandHandler(Plugin plugin, String[] aliases, String description, String usage) {
        this.plugin = plugin;
        this.aliases = aliases;
        this.description = description;
        this.usage = usage;
    }

    /**
     * Safely executes the command from the provided context.
     * @param context contains all necessary and relevant command details.
     * @return a response that should be sent and deleted by the {@link CommandManager} in the source channel.
     */
    Message call(CommandInvocationContext context) {
        //TODO permission check
        try {
            return execute(context);
        } catch (Exception e) {
            logger.severe("Unknown error during the execution of the '" + context.getUsedAlias() + "' command:");
            e.printStackTrace();
            return new MessageBuilder("An unknown error occurred while executing this command.").build(); //TODO formatted message
        }
    }

    /**
     * Method to be implemented by the actual command handlers.
     * @param context contains all relevant command details.
     * @return a response that should be sent and deleted by the {@link CommandManager} in the source channel.
     */
    protected abstract Message execute(CommandInvocationContext context);

    /**
     * @return all aliases this {@link CommandHandler} wants to listen to.
     */
    public String[] getAliases() {
        return aliases;
    }

    /**
     * @return the short description for this command.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the parameter usage code.
     */
    public String getUsage() {
        return usage;
    }
}
