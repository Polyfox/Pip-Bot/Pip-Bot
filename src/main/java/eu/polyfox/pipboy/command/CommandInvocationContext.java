package eu.polyfox.pipboy.command;

import eu.polyfox.pipboy.PipBoy;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

/**
 * Contains parsed information and contextual methods.
 * @author tr808axm
 */
public class CommandInvocationContext {
    private final PipBoy pipBoy;
    private final Message invocationMessage;
    private final String usedPrefix;
    private final String usedAlias;
    private final String[] arguments;
    private CommandHandler handler;

    /**
     * Initializes this data container.
     * @param pipBoy the framework instance.
     * @param invocationMessage the message that invoked this command execution.
     * @param usedPrefix the prefix used in the message.
     * @param usedAlias the used command alias.
     * @param arguments array of command arguments
     */

    private CommandInvocationContext(PipBoy pipBoy, Message invocationMessage, String usedPrefix, String usedAlias, String[] arguments) {
        this.pipBoy = pipBoy;
        this.invocationMessage = invocationMessage;
        this.usedPrefix = usedPrefix;
        this.usedAlias = usedAlias;
        this.arguments = arguments;
    }

    /**
     * @return the message that invoked this command execution.
     */
    public Message getMessage() {
        return invocationMessage;
    }

    /**
     * @return the used prefix.
     */
    public String getUsedPrefix() {
        return usedPrefix;
    }

    /**
     * @return the used alias.
     */
    public String getUsedAlias() {
        return usedAlias;
    }

    /**
     * @return the command arguments (raw invocation message text without prefix and main command).
     */
    public String[] getArguments() {
        return arguments;
    }

    // package-private
    void setHandler(CommandHandler handler) {
        this.handler = handler;
    }

    // ----- Convenience methods -----

    /**
     * Shortcut for {@link #getMessage()}.getGuild().
     * @return the guild this command was executed on. May be null if this was sent in a private channel.
     */
    public Guild getGuild() {
        return invocationMessage.getGuild();
    }

    /**
     * Shortcut for {@link #getMessage()}.getChannel().
     * @return the channel this command was executed in. May be a {@link net.dv8tion.jda.core.entities.TextChannel} or a
     * {@link net.dv8tion.jda.core.entities.PrivateChannel} depending on whether this was sent on a guild or in private.
     */
    public MessageChannel getChannel() {
        return invocationMessage.getChannel();
    }

    /**
     * Shortcut for {@link #getMessage()}.getJDA().
     * @return the JDA instance that maintains the channel this message was sent in.
     */
    public JDA getJDA() {
        return invocationMessage.getJDA();
    }

    /**
     * Shortcut for {@link #getMessage()}.getAuthor().
     * @return the User who invoked this command.
     */
    public User getInvoker() {
        return invocationMessage.getAuthor();
    }

    /**
     * Parses a raw message into command components.
     * @param message the discord message object to parse.
     * @return an object with the parsed arguments or null if the message does not start with an accepted prefix.
     */
    public static CommandInvocationContext parse(PipBoy pipBoy, Message message) {
        String prefix = null;
        // mentions
        if (message.getContentRaw().startsWith(message.getJDA().getSelfUser().getAsMention()))
            prefix = message.getJDA().getSelfUser().getAsMention();
        // default prefix
        else if (message.getContentRaw().toLowerCase().startsWith(pipBoy.getDefaultPrefix().toLowerCase()))
            prefix = message.getContentRaw().substring(0, pipBoy.getDefaultPrefix().length()); // keep lower/upper case
        //TODO guild prefixes?

        if(prefix != null) {
            // cut off command prefix and remove whitespaces at the beginning and the end
            String beheaded = message.getContentRaw().substring(prefix.length(), message.getContentRaw().length()).trim();
            // split arguments
            String[] allArguments = beheaded.split("\\s+"); // one or multiple whitespaces
            // create an array of the actual command arguments (exclude invocation arg)
            String[] args = new String[allArguments.length - 1];
            System.arraycopy(allArguments, 1, args, 0, args.length);
            return new CommandInvocationContext(pipBoy, message, prefix, allArguments[0], args);

        }
        return null;
    }
}
