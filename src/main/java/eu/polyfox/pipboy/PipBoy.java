package eu.polyfox.pipboy;

import eu.polyfox.pipboy.command.CommandManager;
import eu.polyfox.pipboy.plugin.Plugin;
import eu.polyfox.pipboy.plugin.PluginLoader;
import eu.polyfox.pipboy.plugin.PluginManager;
import eu.polyfox.pipboy.plugin.PluginManagerImpl;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * Pip-Boys main class.
 * @author tr808axm
 */
public class PipBoy {
    private static final File DEFAULT_PLUGINS_DIRECTORY = new File("./plugins/");
    private final Logger logger = Logger.getLogger("Pip-Boy");
    private final JDA jda;
    private final CommandManager commandManager;

    /**
     * Entry-point for this application.
     * @param args command line parameters.
     */
    public static void main(String[] args) {
        try {
            new PipBoy(args.length == 0 ? null : args[0]);
        } catch (Exception e) {
            throw new RuntimeException("Could not initialize Pip-Boy:", e);
        }
    }

    /**
     * Constructs Pip-Boy and logs in the discord bot.
     * @param token the token to login with.
     * @throws LoginException if the discord bot token was incorrect.
     * @throws InterruptedException if the Thread was interrupted while logging in to discord.
     */
    public PipBoy(String token) throws LoginException, InterruptedException, IOException {
        // create plugins directory if necessary
        File pluginsDirectory = DEFAULT_PLUGINS_DIRECTORY;
        if(!pluginsDirectory.exists() && !pluginsDirectory.mkdirs())
            throw new IOException("Could not create or read plugins directory at " + pluginsDirectory.getAbsolutePath());

        // load plugins from plugins directory
        List<Plugin> plugins = PluginLoader.loadPlugins(pluginsDirectory);

        // init plugins
        PluginManager manager = new PluginManagerImpl(this);
        for (Plugin plugin : plugins)
            plugin.setPluginManager(manager);

        // load and init JDA
        JDABuilder builder = new JDABuilder(AccountType.BOT);
        builder.setToken(token);
        this.jda = builder.buildBlocking();
        logger.info("Login successful!");

        commandManager = new CommandManager(this);

        // enable plugins
        for (Plugin plugin : plugins)
            plugin.enable();

        Thread.sleep(330 * 1000);

        for (Plugin plugin : plugins)
            plugin.disable();

        jda.shutdownNow();
    }

    /**
     * @return the JDA instance for the bot.
     */
    public JDA getJda() {
        return jda;
    }

    /**
     * @return the command manager.
     */
    public CommandManager getCommandManager() {
        return commandManager;
    }

    public String getDefaultPrefix() {
        return "pipboy ";
    }
}
