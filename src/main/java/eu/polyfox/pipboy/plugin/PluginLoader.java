package eu.polyfox.pipboy.plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.logging.Logger;

/**
 * Loads all plugins from a specified folder.
 * @author tr808axm
 */
public class PluginLoader {
    private static final Logger logger = Logger.getLogger(PluginLoader.class.getName());

    /**
     * Loads all {@link Plugin Plugins} from a folder.
     * @param pluginDir the folder to load plugins from.
     * @return all found plugin implementations, already initialized (call {@link Plugin#enable())!).
     * @throws IOException if any io exceptions occur.
     */
    public static List<Plugin> loadPlugins(File pluginDir) throws IOException {
        File[] pluginJars = pluginDir.listFiles(f -> f.getName().toLowerCase().endsWith(".jar"));
        ClassLoader loader = new URLClassLoader(fileArrayToURLArray(pluginJars));
        List<Class<Plugin>> pluginClasses = PluginLoader.extractClassesFromJARs(pluginJars, loader);
        return createPluginObjects(pluginClasses);
    }

    private static List<Plugin> createPluginObjects(List<Class<Plugin>> pluginClasses) {
        List<Plugin> plugins = new ArrayList<>(pluginClasses.size());
        for (Class<Plugin> pluginClass : pluginClasses) {
            try {
                plugins.add(pluginClass.newInstance());
            } catch (IllegalAccessException e) {
                logger.severe("IllegalAccessException for plugin class " + pluginClass.getName());
                e.printStackTrace();
            } catch (InstantiationException e) {
                logger.severe("Can not instantiate plugin class " + pluginClass.getName());
                e.printStackTrace();
            }
        }
        return plugins;
    }

    private static List<Class<Plugin>> extractClassesFromJARs(File[] jars, ClassLoader loader) throws IOException {
        List<Class<Plugin>> pluginClasses = new ArrayList<>();
        for (File jar : jars)
            pluginClasses.addAll(PluginLoader.extractClassesFromJAR(jar, loader));
        return pluginClasses;
    }

    /**
     * Extracts all classes implementing {@link Plugin} from a jar {@link File}.
     * @param jar the container to extract classes from.
     * @param loader the class loader that has the jar file in it's classpath.
     * @return the classes implementing {@link Plugin} as a list.
     * @throws IOException if any IO problem occurs.
     */
    @SuppressWarnings("unchecked")
    private static List<Class<Plugin>> extractClassesFromJAR(File jar, ClassLoader loader) throws IOException {
        List<Class<Plugin>> pluginClasses = new ArrayList<>();
        JarInputStream jarIn = new JarInputStream(new FileInputStream(jar));
        JarEntry entry = null;
        while ((entry = jarIn.getNextJarEntry()) != null) {
            if(entry.getName().toLowerCase().endsWith(".class")) {
                try {
                    Class<?> clazz = loader.loadClass(entry.getName().substring(0, entry.getName().length() - 6).replace('/', '.'));
                    if(PluginLoader.isPluginClass(clazz))
                        pluginClasses.add((Class<Plugin>) clazz);
                } catch (ClassNotFoundException e) {
                    logger.severe("Can not load class " + entry.getName());
                    e.printStackTrace();
                }
            }
        }
        jarIn.close();
        return pluginClasses;
    }

    /**
     * Checks whether a class extends / implements the {@link Plugin} class.
     * @param clazz the class to check.
     * @return true if clazz implements {@link Plugin}.
     */
    private static boolean isPluginClass(Class<?> clazz) {
        for (Class<?> interfaceClass : clazz.getInterfaces())
            if(interfaceClass.equals(Plugin.class))
                return true;
        return false;
    }

    /**
     * Packs all urls of a {@link File} array into a {@link URL} array.
     * @param files the files to convert.
     * @return the url array.
     * @throws MalformedURLException if a file URL is malformed.
     */
    private static URL[] fileArrayToURLArray(File[] files) throws MalformedURLException {
        URL[] urls = new URL[files.length];
        for (int i = 0; i < files.length; i++)
            urls[i] = files[i].toURI().toURL();
        return urls;
    }
}
