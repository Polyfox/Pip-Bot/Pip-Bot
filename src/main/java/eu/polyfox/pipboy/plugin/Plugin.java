package eu.polyfox.pipboy.plugin;

/**
 * Interface for Pip-Boy plugins to be implemented in external JARs.
 * @author tr808axm
 */
public interface Plugin {
    /**
     * Called when the plugin is enabled.
     */
    void enable();

    /**
     * Called when the plugin is disabled.
     */
    void disable();

    /**
     * Sets the plugin manager of the class.
     * @param pluginManager the plugin manager that manages this plugin.
     */
    void setPluginManager(PluginManager pluginManager);
}
