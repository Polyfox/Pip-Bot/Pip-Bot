package eu.polyfox.pipboy.plugin;

import eu.polyfox.pipboy.PipBoy;

/**
 * Manages plugin instances.
 * @author tr808axm
 */
public interface PluginManager {
    /**
     * @return the PipBoy instance.
     */
    PipBoy getPipBoy();
}
