package eu.polyfox.pipboy.plugin;

import eu.polyfox.pipboy.PipBoy;

/**
 * {@inheritDoc}
 * @author tr808axm
 */
public class PluginManagerImpl implements PluginManager {
    private final PipBoy pipBoy;

    public PluginManagerImpl(PipBoy pipBoy) {
        this.pipBoy = pipBoy;
    }

    @Override
    public PipBoy getPipBoy() {
        return pipBoy;
    }
}
